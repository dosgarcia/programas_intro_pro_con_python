# -*- coding: utf-8 -*-
a = int(raw_input('Dame el 1er numero '))
b = int(raw_input('Dame el 2do numero '))
c = int(raw_input('Dame el 3er numero '))
d = int(raw_input('Dame el 4to numero '))
e = int(raw_input('Dame el 5to numero '))

"""
Debo restar los 4 ultimos numeros con el primero, 
porque mi algoritmo me dira que el numero mas bajito, 
sera el mas cercano, siempre y cuando los resultados 
sean positivos
"""

z1 = a - b
z2 = a - c
z3 = a - d
z4 = a - e

"""Luego debo hacer que la resta de todos los numeros 
sean positivos, porque si son negativos el algoritmo fallara."""

zp1 = abs(z1)
zp2 = abs(z2)
zp3 = abs(z3)
zp4 = abs(z4)

"""Aqui busco el numero menor, ya que este numero
sera el mas cercano al numero dado... y como debo 
saber cual es el numero original lo establezco en 
la variable numero_mas _cercano, para luego mostrarlo"""

candidato = zp1
numero_mas_cercano = b
if zp2 < candidato:
    candidato = zp2
    numero_mas_cercano = c
if zp3 < candidato:
    candidato = zp3
    numero_mas_cercano = d
if zp4 < candidato:
    candidato = zp4
    numero_mas_cercano = e
maximo = candidato


print 'el numero mas cercado es', numero_mas_cercano

