# -*- coding: utf-8 -*-
from math import pi

radio = float(raw_input('Dame el radio de un circulo: '))

opcion = ''
#menu
while opcion < 'a' or opcion > 'c':
    print 'Escoge una opcion: '
    print 'a) Calcular el diametro. '
    print 'b) Calcular el perimetro. '
    print 'c) Calcular el area. '

    opcion = raw_input('Teclea a, b o c y pulsa enter: ')
    opcion = opcion.lower()


    if opcion == 'a': #  Calculo el diametro
        diametro = 2 * radio
        print 'El diametro es', diametro
    elif opcion == 'b':  # Calculo el perimetro
            perimetro = 2 * pi * radio
            print 'El perimetro es', perimetro
    elif opcion == 'c':  # Calculo el area
                area = pi * radio ** 2
                print 'El area es', area
    else:
        print 'Solo hay tres opciones: a, b o c. '
        print 'Tu has tecleado', opcion